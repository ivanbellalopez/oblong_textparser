//
//  TPParserManager.h
//  textParser
//
//  Created by Ivan Bella López on 13/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TPAppController.h"

@class TPWord;

@protocol TPParserManagerDelegate <NSObject>

- (void)parserDidUpdateWord:(TPWord *)word;

@end

@interface TPParserManager : NSObject

- (instancetype)initWithDelegate: (id <TPParserManagerDelegate>)delegate;
- (void)parseData:(NSData *)data;

@end

//
//  TPTextPickerViewController.m
//  textParser
//
//  Created by Ivan Bella López on 13/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "TPTextPickerViewController.h"
#import "NSString+Additions.h"
#import "TPAppController.h"
#import "WYPopoverController.h"
#import "TPFilePickerTableViewController.h"

@interface TPTextPickerViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UIButton *loadURLButton;
@property (nonatomic, weak) IBOutlet UIButton *loadFileButton;
@property (nonatomic, weak) IBOutlet UITextField *textField;
@property (nonatomic, strong) WYPopoverController *popoverViewController;

@end

@implementation TPTextPickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


#pragma mark Actions

- (IBAction)loadURLAction:(id)sender {
    [self _loadURLWithText:self.textField.text];
}

- (IBAction)loadFileAction:(id)sender {
    [self.textField resignFirstResponder];
    
    TPFilePickerTableViewController *filePickerViewController = [TPFilePickerTableViewController new];
    self.popoverViewController = [[WYPopoverController alloc] initWithContentViewController:filePickerViewController];
    filePickerViewController.parentController = self.popoverViewController;

    self.popoverViewController.popoverLayoutMargins = UIEdgeInsetsMake(0, 20, 100, 20);
    [self.popoverViewController presentPopoverFromRect:self.loadFileButton.bounds inView:self.loadFileButton permittedArrowDirections:WYPopoverArrowDirectionUp animated:YES];
}


#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([string isEqualToString:@"\n"]) {
        [self _loadURLWithText:textField.text];
    }
    
    return YES;
}


#pragma mark Private

- (void)_loadURLWithText:(NSString *)urlString {
    if ([urlString length] == 0) {
        urlString = self.textField.placeholder;
    }
    
    if (![urlString validateURL]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"The address is not valid.\nCheck the format." delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles: nil];
        
        [alertView show];
    } else {
        [self.textField resignFirstResponder];
        [AppController() requestTextWithURL:urlString];
    }
}


@end

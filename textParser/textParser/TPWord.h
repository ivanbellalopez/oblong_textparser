//
//  TPWord.h
//  textParser
//
//  Created by Ivan Bella López on 14/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TPWord : NSObject

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) NSString *word;
@property (nonatomic, assign) NSInteger amount;

- (instancetype)initWithWord:(NSString *)word amount:(NSInteger)amount index:(NSInteger)index;

@end

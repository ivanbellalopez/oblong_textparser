//
//  TPWord.m
//  textParser
//
//  Created by Ivan Bella López on 14/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "TPWord.h"

@implementation TPWord

- (instancetype)initWithWord:(NSString *)word amount:(NSInteger)amount index:(NSInteger)index;
{
    if (self = [super init]) {
        self.word = word;
        self.amount = amount;
        self.index = index;
    }
    
    return self;
}

@end

//
//  NSString+Additions.h
//  textParser
//
//  Created by Ivan Bella López on 14/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)
- (BOOL)validateURL;
- (NSString *)urlPathName;
@end

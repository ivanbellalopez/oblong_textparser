//
//  TPFileManager.m
//  textParser
//
//  Created by Ivan Bella López on 15/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "TPFileManager.h"
#import "TPFileManager+Notifications.h"
#import "NSString+Additions.h"

@interface TPFileManager () <NSStreamDelegate>

@property (nonatomic, weak) id <TPFileManagerDelegate> delegate;
@property (nonatomic, strong) NSOutputStream *outputStream;
@property (nonatomic, strong) NSInputStream *inputStream;
@property (nonatomic, strong) NSString *filePath;
@property (nonatomic, strong) dispatch_queue_t serialQueue;

@end


@implementation TPFileManager

- (instancetype)initWithDelegate:(id<TPFileManagerDelegate>)delegate {
    if (self = [super init]) {
        self.delegate = delegate;
        [self registerForNotifications];
    }
    
    self.serialQueue = dispatch_queue_create("texparser.queue.file.manager", DISPATCH_QUEUE_SERIAL);
    
    return self;
}


#pragma Stream handler

- (void)createOutputStreamWithFilename:(NSString *)filename {
    self.filePath = [[self applicationDocumentsDirectory].path stringByAppendingPathComponent:[filename urlPathName]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:self.filePath]) {
        self.outputStream = [[NSOutputStream alloc] initToFileAtPath:self.filePath append:NO];
        [self.outputStream setDelegate:self];
        [self.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [self.outputStream open];
    }
}

- (void)createInputStreamWithFilename:(NSString *)filename {
    NSString *path = [[self applicationDocumentsDirectory].path stringByAppendingPathComponent:filename];
    
    self.inputStream = [[NSInputStream alloc] initWithFileAtPath:path];
    [self.inputStream setDelegate:self];
    [self.inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [self.inputStream open];
}

- (void)closeStream {
    
    dispatch_async(self.serialQueue, ^{
        @synchronized(self) {
            if (self.outputStream) {
                [self.outputStream close];
                [self.outputStream removeFromRunLoop:[NSRunLoop currentRunLoop]
                                             forMode:NSDefaultRunLoopMode];
                self.outputStream = nil;
            }
            
            if (self.inputStream) {
                [self.inputStream close];
                [self.inputStream removeFromRunLoop:[NSRunLoop currentRunLoop]
                                            forMode:NSDefaultRunLoopMode];
                self.inputStream = nil;
            }
        }    });
    
    
}


#pragma mark NSStreamDelegate

- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode {
    switch(eventCode) {
        case NSStreamEventOpenCompleted:
            if (aStream == self.inputStream) {
                [self readStream];
            }
            
            break;
            
        case NSStreamEventHasSpaceAvailable:
            break;
            
        case NSStreamEventHasBytesAvailable:
            break;
            
        case NSStreamEventEndEncountered:
            [self closeStream];
            break;
            
        case NSStreamEventErrorOccurred:
            [self closeStream];
            [self deleteCurrentOutputStreamFile];
            break;
            
        case NSStreamEventNone:
            break;
    }
}


#pragma mark I/O

- (void)appendData:(NSData *)data {
    if (!self.outputStream) {
        return;
    }
    dispatch_async(self.serialQueue, ^{
        @synchronized(self) {
            const uint8_t *buf = [data bytes];
            NSUInteger length = [data length];
            NSInteger nwritten = [self.outputStream write:buf maxLength:length];
            NSLog(@"written bytes: %li", (long)nwritten);
        }
    });
}

- (void)readStream {
    dispatch_async(self.serialQueue, ^{
        
        uint8_t buffer[4096];
        NSInteger len;
        
        while ([self.inputStream hasBytesAvailable]) {
            len = [self.inputStream read:buffer maxLength:sizeof(buffer)];
            if (len > 0) {
                if ([self.delegate respondsToSelector:@selector(fileManagerDidReadData:)]) {
                    [self.delegate fileManagerDidReadData:[NSData dataWithBytes:buffer length:len]];
                    NSLog(@"read bytes: %li", (long)len);
                }
            }
        }
    });
}


#pragma mark File management

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

- (NSArray *)listSavedFiles {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[paths lastObject] error:NULL];
    return directoryContent;
}

- (void)deleteCurrentOutputStreamFile {
    if (self.outputStream) {
        [[NSFileManager defaultManager] removeItemAtPath:self.filePath error:nil];
    }
    
    [self closeStream];
}

@end

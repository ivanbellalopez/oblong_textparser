//
//  TPFilePickerTableViewController.h
//  textParser
//
//  Created by Ivan Bella López on 15/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYPopoverController;

@interface TPFilePickerTableViewController : UITableViewController

@property (nonatomic, strong) WYPopoverController *parentController;

@end

//
//  TPNetworkManager+Errors.m
//  textParser
//
//  Created by Ivan Bella López on 13/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "TPNetworkManager+Errors.h"

@implementation TPNetworkManager (Errors)

- (void)showAlertWithError:(NSError *)error
{
    NSString *message = @"";
    if (error.code == 200) {
        message = @"The URL is not plain text.";
    } else {
        message = error.localizedDescription;
    }
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    
    [alertView show];
}

@end

//
//  TPParserManager.m
//  textParser
//
//  Created by Ivan Bella López on 13/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "TPParserManager.h"
#import "TPParserManager+Notifications.h"
#import "TPWord.h"

@interface TPParserManager ()
@property (nonatomic, weak) id <TPParserManagerDelegate> delegate;
@property (nonatomic, assign) NSInteger wordIndex;
@property (nonatomic, strong) NSMutableDictionary *wordsDictionary;
@end


@implementation TPParserManager

- (instancetype)initWithDelegate:(id<TPParserManagerDelegate>)delegate {
    if (self = [super init]) {
        self.delegate = delegate;
        [self registerForNotifications];
    }
    
    return self;
}


#pragma mark Public

- (void)resetParser {
    [self.wordsDictionary removeAllObjects];
    self.wordIndex = 0;
}

- (void)parseData:(NSData *)data {
    @synchronized(self) {
        if (!self.wordsDictionary) {
            self.wordsDictionary = [NSMutableDictionary new];
        }
        
        NSString *tmpStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        [self _splitStringIntoWords:tmpStr];
    }
}


#pragma mark Private

- (void)_splitStringIntoWords:(NSString *)string {
    NSString *alphabeticalString = [[string componentsSeparatedByCharactersInSet:
                                     [[NSCharacterSet letterCharacterSet] invertedSet]] componentsJoinedByString:@" "];
    
    [[alphabeticalString lowercaseString] enumerateSubstringsInRange:NSMakeRange(0, [string length])
                                                             options:NSStringEnumerationByWords
                                                          usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                                                              [self _updateWordsDictionaryWithString:substring];
                                                          }];
}

- (void)_updateWordsDictionaryWithString:(NSString *)string {
    TPWord *word = nil;
    @synchronized(word) {
        
        if (self.wordsDictionary[string]) {
            word = self.wordsDictionary[string];
            word.amount += 1;
        } else {
            word = [[TPWord alloc] initWithWord:string amount:1 index:self.wordIndex];
            self.wordIndex++;
        }
        
        [self.wordsDictionary setObject:word forKey:string];
        
        if ([self.delegate respondsToSelector:@selector(parserDidUpdateWord:)]) {
            [self.delegate parserDidUpdateWord:word];
        }
    }
}

@end

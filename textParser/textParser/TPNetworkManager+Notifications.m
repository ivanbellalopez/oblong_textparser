//
//  TPNetworkManager+Notifications.m
//  textParser
//
//  Created by Ivan Bella López on 16/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "TPNetworkManager+Notifications.h"

@interface TPNetworkManager ()

- (void)requestWithURL:(NSString *)urlString;

@end


@implementation TPNetworkManager (Notifications)

- (void)registerForNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_requestURL:) name:TPNotificationWillAttemptLoad object:nil];
}

- (void)_requestURL:(NSNotification *)notification {
    if ([notification.userInfo[@"type"] isEqualToString:@"URL"]) {
        [self requestWithURL:notification.userInfo[@"resource"]];
    }
}

@end
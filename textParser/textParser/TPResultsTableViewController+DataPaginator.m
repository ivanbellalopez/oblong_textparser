//
//  TPResultsTableViewController+DataPaginator.m
//  textParser
//
//  Created by Ivan Bella López on 14/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "TPResultsTableViewController+DataPaginator.h"
#import "TPWord.h"

static const NSInteger PageSize = 80;

@interface TPResultsTableViewController ()

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSMutableArray *loadedItems;
@property (nonatomic, assign) NSInteger itemsAtPage;
@property (nonatomic, assign) TPSortingType sortingType;

- (void)configureCell:(UITableViewCell *)cell forWord:(TPWord *)wordItem;

@end

@implementation TPResultsTableViewController (DataPaginator)


#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    if (maximumOffset - currentOffset <= 3 * self.tableView.rowHeight
        && [self.items count] > [self.tableView numberOfRowsInSection:0]) {
        [self loadItemsIfNeeded];
    }
}


#pragma mark Pagination

- (void)loadItemsIfNeeded {
    @synchronized(self) {
        NSInteger loc = [self.loadedItems count] - 1;
        NSInteger len = MIN(PageSize, [self.items count] - [self.loadedItems count]);
        NSArray *itemsBatch = [self.items subarrayWithRange:NSMakeRange(loc, len)];
        
        for (TPWord *aWord in itemsBatch) {
            [self _insertItemAtIndexBySortingType:aWord];
        }
    }
    
    [self.tableView reloadData];
    self.itemsAtPage = 0;
}

- (void)loadNextSingleItemIfNeeded {
    if (self.itemsAtPage > PageSize)
        return;
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self.tableView beginUpdates];
        [self _insertItemAtIndexBySortingType:[self.items lastObject]];
        
        NSInteger row = [self.loadedItems count] - 1;
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]]
                              withRowAnimation:UITableViewRowAnimationFade];
        
        self.itemsAtPage ++;
        [self.tableView endUpdates];
    });
}

- (void)updateItemIfNeeded:(TPWord *)word {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self.tableView indexPathsForVisibleRows] enumerateObjectsUsingBlock:^(NSIndexPath *indexPath, NSUInteger idx, BOOL *stop) {
            if (word.index == idx) {
                *stop = YES;
                UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                [self configureCell:cell forWord:word];
            }
        }];
    });
}

- (void)_insertItemAtIndexBySortingType:(TPWord *)newWord {
    NSComparator comparator = nil;
    if (self.sortingType == TPSortingTypeDefault) {
        comparator = ^NSComparisonResult(TPWord *a, TPWord *b) {
            NSNumber *aNumber = [NSNumber numberWithInteger:a.index];
            NSNumber *bNumber = [NSNumber numberWithInteger:b.index];
            return [aNumber compare: bNumber];
        };
    } else if (self.sortingType == TPSortingTypeAlphabetical) {
        comparator = ^NSComparisonResult(TPWord *a, TPWord *b) {
            return [a.word localizedCaseInsensitiveCompare: b.word];
        };
    } else if (self.sortingType == TPSortingTypeWordCount) {
        comparator = ^NSComparisonResult(TPWord *a, TPWord *b) {
            NSNumber *aNumber = [NSNumber numberWithInteger:a.amount];
            NSNumber *bNumber = [NSNumber numberWithInteger:b.amount];
            return [aNumber compare: bNumber];
        };
    }
    
    @synchronized(self.loadedItems) {
        NSUInteger index = [self.loadedItems indexOfObject:newWord
                                             inSortedRange:(NSRange){0, [self.loadedItems count]}
                                                   options:NSBinarySearchingInsertionIndex
                                           usingComparator:comparator];
        
        [self.loadedItems insertObject:newWord atIndex:index];
    }
}


- (void)resetPaginator {
    self.itemsAtPage = 0;
}

@end

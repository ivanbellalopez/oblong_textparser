//
//  TPResultsTableViewController.h
//  textParser
//
//  Created by Ivan Bella López on 13/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPAppController.h"

typedef NS_ENUM(NSUInteger, TPSortingType) {
    TPSortingTypeAlphabetical,
    TPSortingTypeWordCount,
    TPSortingTypeDefault
};

@interface TPResultsTableViewController : UIViewController

@end

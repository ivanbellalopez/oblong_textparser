//
//  TPAppController.h
//  textParser
//
//  Created by Ivan Bella López on 13/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPNetworkManager, TPParserManager, TPWord;

extern NSString* const TPNotificationDidFinishReceivingData;
extern NSString* const TPNotificationWillAttemptLoad;
extern NSString* const TPNotificationNetworkNotvalid;

@protocol TPAppControllerDelegate <NSObject>
- (void)appControllerDidReceiveUpdatedWord:(TPWord *)word;
@end


@interface TPAppController : NSObject
+ (TPAppController *)sharedInstance;
@property (nonatomic, weak) id <TPAppControllerDelegate> delegate;
- (void)requestTextWithURL:(NSString *)urlString;
- (NSArray *)requestSavedFilesList;
- (void)loadFilename:(NSString *)filename;
@end

static inline TPAppController * AppController() { return [TPAppController sharedInstance]; }

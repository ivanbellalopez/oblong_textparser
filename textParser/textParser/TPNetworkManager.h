//
//  TPNetworkManager.h
//  textParser
//
//  Created by Ivan Bella López on 13/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TPAppController.h"


@protocol TPNetworkManagerProtocol <NSObject>

- (void)networkDidReceiveData:(NSData *)data;
- (void)networkDidFinishReceivingData:(NSError *)error;
- (void)networkURLNotValid;

@end


@interface TPNetworkManager : NSObject <NSURLSessionDataDelegate>

- (instancetype)initWithDelegate: (id <TPNetworkManagerProtocol>)delegate;

@end


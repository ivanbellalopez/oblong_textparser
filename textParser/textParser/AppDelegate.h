//
//  AppDelegate.h
//  textParser
//
//  Created by Ivan Bella López on 12/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


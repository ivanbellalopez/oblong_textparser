//
//  TPFilePickerTableViewController.m
//  textParser
//
//  Created by Ivan Bella López on 15/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "TPFilePickerTableViewController.h"
#import "TPAppController.h"
#import "WYPopoverController.h"

@interface TPFilePickerTableViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *savedFilesArray;

@end

@implementation TPFilePickerTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.savedFilesArray = [AppController() requestSavedFilesList];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.savedFilesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"FilePickerCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = self.savedFilesArray[[indexPath row]];
        return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.parentController dismissPopoverAnimated:YES];
    [AppController() loadFilename:self.savedFilesArray[[indexPath row]]];
}


@end

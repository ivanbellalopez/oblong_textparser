//
//  main.m
//  textParser
//
//  Created by Ivan Bella López on 12/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

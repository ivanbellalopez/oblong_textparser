//
//  TPFileManager+Notifications.m
//  textParser
//
//  Created by Ivan Bella López on 16/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "TPFileManager+Notifications.h"

@interface TPFileManager ()

- (void)createOutputStreamWithFilename:(NSString *)filename;
- (void)createInputStreamWithFilename:(NSString *)filename;
- (void)closeStream;
- (void)deleteCurrentOutputStreamFile;
@end


@implementation TPFileManager (Notifications)

- (void)registerForNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_createStreams:) name:TPNotificationWillAttemptLoad object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_resetStreams:) name:TPNotificationDidFinishReceivingData object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_deleteFile:) name:TPNotificationNetworkNotvalid object:nil];
}

- (void)_createStreams:(NSNotification *)notification {
    if ([notification.userInfo[@"type"] isEqualToString:@"file"]) {
        [self createInputStreamWithFilename:notification.userInfo[@"resource"]];
    } else {
        [self createOutputStreamWithFilename:notification.userInfo[@"resource"]];
    }
    
}

- (void)_resetStreams:(NSNotification *)notification {
    [self closeStream];
}

- (void)_deleteFile:(NSNotification *)notification {
    [self deleteCurrentOutputStreamFile];
}

@end

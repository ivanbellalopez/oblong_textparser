//
//  TPNetworkManager+Notifications.h
//  textParser
//
//  Created by Ivan Bella López on 16/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "TPNetworkManager.h"

@interface TPNetworkManager (Notifications)

- (void)registerForNotifications;

@end

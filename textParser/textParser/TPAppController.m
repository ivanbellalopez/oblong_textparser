//
//  TPAppController.m
//  textParser
//
//  Created by Ivan Bella López on 13/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "TPAppController.h"
#import "TPNetworkManager.h"
#import "TPParserManager.h"
#import "TPFileManager.h"

NSString* const TPNotificationDidFinishReceivingData = @"TPNotificationDidFinishReceivingData";
NSString* const TPNotificationWillAttemptLoad = @"TPNotificationWillAttemptLoad";
NSString* const TPNotificationNetworkNotvalid = @"TPNotificationNetworkNotvalid";


@interface TPAppController () <TPNetworkManagerProtocol , TPParserManagerDelegate, TPFileManagerDelegate>
@property (nonatomic, strong) TPNetworkManager *networkManager;
@property (nonatomic, strong) TPParserManager *parserManager;
@property (nonatomic, strong) TPFileManager *fileManager;
@end

@implementation TPAppController

#pragma mark Singleton

+ (TPAppController *)sharedInstance {
    static TPAppController *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[TPAppController alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype)init
{
    if (self = [super init]) {
        self.networkManager = [[TPNetworkManager alloc] initWithDelegate:self];
        self.parserManager = [[TPParserManager alloc] initWithDelegate:self];
        self.fileManager = [[TPFileManager alloc] initWithDelegate:self];
    }
    
    return self;
}


#pragma mark Public

- (void)requestTextWithURL:(NSString *)urlString {
    [[NSNotificationCenter defaultCenter] postNotificationName:TPNotificationWillAttemptLoad
                                                        object:nil
                                                      userInfo:@{@"type" : @"URL", @"resource" : urlString}];
}

- (void)loadFilename:(NSString *)filename {
    [[NSNotificationCenter defaultCenter] postNotificationName:TPNotificationWillAttemptLoad
                                                        object:nil
                                                      userInfo:@{@"type" : @"file", @"resource" : filename}];
}

- (NSArray *)requestSavedFilesList {
    return [self.fileManager listSavedFiles];
}


#pragma mark TPNetworkManagerProtocol

- (void)networkDidReceiveData:(NSData *)data {
    [self.parserManager parseData:data];
    [self.fileManager appendData:data];
}

- (void)networkDidFinishReceivingData:(NSError *)error {
    [[NSNotificationCenter defaultCenter] postNotificationName:TPNotificationDidFinishReceivingData object:nil];
}

- (void)networkURLNotValid {
    [[NSNotificationCenter defaultCenter] postNotificationName:TPNotificationNetworkNotvalid object:nil];
}


#pragma mark TPParserManagerDelegate

- (void)parserDidUpdateWord:(TPWord *)word
{
    if ([self.delegate respondsToSelector:@selector(appControllerDidReceiveUpdatedWord:)]) {
        [self.delegate appControllerDidReceiveUpdatedWord:word];
    }
}


#pragma mark TPFileManagerDelegate

- (void)fileManagerDidReadData:(NSData *)data
{
    [self.parserManager parseData:data];
}

@end

//
//  TPResultsTableViewController+Notifications.m
//  textParser
//
//  Created by Ivan Bella López on 16/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "TPResultsTableViewController+Notifications.h"

@interface TPResultsTableViewController ()

- (void)resetToDefaults;

@end


@implementation TPResultsTableViewController (Notifications)

- (void)registerForNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_resetToDefaults:) name:TPNotificationWillAttemptLoad object:nil];
}

- (void)_resetToDefaults:(NSNotification *)notification {
    [self resetToDefaults];
}

@end

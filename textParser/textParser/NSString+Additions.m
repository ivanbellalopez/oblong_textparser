//
//  NSString+Additions.m
//  textParser
//
//  Created by Ivan Bella López on 14/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "NSString+Additions.h"

@implementation NSString (Additions)

- (BOOL)validateURL {
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:self];
}

- (NSString *)urlPathName
{
    return [[self componentsSeparatedByString:@"/"] lastObject];
}

@end

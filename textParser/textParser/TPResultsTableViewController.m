//
//  TPResultsTableViewController.m
//  textParser
//
//  Created by Ivan Bella López on 13/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "TPResultsTableViewController.h"
#import "TPResultsTableViewController+Notifications.h"
#import "TPResultsTableViewController+DataPaginator.h"
#import "TPWord.h"


@interface TPResultsTableViewController () <TPAppControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentControl;

@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSMutableArray *loadedItems;
@property (nonatomic, assign) NSInteger itemsAtPage;

@property (nonatomic, assign) TPSortingType sortingType;

@end

@implementation TPResultsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    AppController().delegate = self;
    
    [self registerForNotifications];
    self.items = [@[] mutableCopy];
    self.loadedItems = [@[] mutableCopy];
    self.sortingType = TPSortingTypeDefault;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark Reset

- (void)resetToDefaults {
    [self.loadedItems removeAllObjects];
    [self.items removeAllObjects];
    [self.tableView reloadData];
    [self resetPaginator];
    [self.segmentControl setSelectedSegmentIndex:0];
    self.sortingType = TPSortingTypeDefault;
}


#pragma mark Actions

- (IBAction)segmentSwitch:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        self.sortingType = TPSortingTypeDefault;
        
        NSArray *tmpArray = [self.loadedItems sortedArrayUsingComparator:^NSComparisonResult(TPWord *a, TPWord *b) {
            NSNumber *aNumber = [NSNumber numberWithInteger:a.index];
            NSNumber *bNumber = [NSNumber numberWithInteger:b.index];
            return [aNumber compare: bNumber];
        }];
        
        self.loadedItems = [tmpArray mutableCopy];
    } else if (selectedSegment == 1) {
        self.sortingType = TPSortingTypeWordCount;
        
        NSArray *tmpArray = [self.loadedItems sortedArrayUsingComparator:^NSComparisonResult(TPWord *a, TPWord *b) {
            NSNumber *aNumber = [NSNumber numberWithInteger:a.amount];
            NSNumber *bNumber = [NSNumber numberWithInteger:b.amount];
            return [aNumber compare: bNumber];
        }];
        
        self.loadedItems = [tmpArray mutableCopy];
        
    } else {
        self.sortingType = TPSortingTypeAlphabetical;
        
        NSArray *tmpArray = [self.loadedItems sortedArrayUsingComparator:^NSComparisonResult(TPWord *a, TPWord *b) {
            return [a.word localizedCaseInsensitiveCompare: b.word];
        }];
        
        self.loadedItems = [tmpArray mutableCopy];
    }
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}


#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.loadedItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ResultsCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    TPWord *wordItem = self.loadedItems[[indexPath row]];
    [self configureCell:cell forWord:wordItem];
    
    UIColor *evenColor = [UIColor lightGrayColor];
    UIColor *oddColor  = [UIColor clearColor];
    cell.backgroundColor  = (indexPath.row % 2) ? evenColor : oddColor;
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma mark Cell layout

- (void)configureCell:(UITableViewCell *)cell forWord:(TPWord *)wordItem {
    NSString *amountString = [@(wordItem.amount) stringValue];
    NSString *composedString = [NSString stringWithFormat:@"%@ → %@", wordItem.word, amountString];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    UIFont * labelFont = [UIFont systemFontOfSize:24.0];
    UIColor * labelColor = [UIColor colorWithWhite:1 alpha:1];
    NSShadow *shadow = [[NSShadow alloc] init];
    [shadow setShadowColor : [UIColor blackColor]];
    [shadow setShadowOffset : CGSizeMake (1.0, 1.0)];
    [shadow setShadowBlurRadius : 2];
    
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString : composedString
                                                                     attributes : @{
                                                                                    NSParagraphStyleAttributeName : paragraphStyle,
                                                                                    NSKernAttributeName : @2.0,
                                                                                    NSFontAttributeName : labelFont,
                                                                                    NSForegroundColorAttributeName : labelColor,
                                                                                    NSShadowAttributeName : shadow }];
    
    cell.textLabel.attributedText = attrString;
}


#pragma mark TPAppControllerDelegate

- (void)appControllerDidReceiveUpdatedWord:(TPWord *)word {
    @synchronized(word) {
        if (word.amount > 1) {
            [self.items replaceObjectAtIndex:word.index withObject:word];
            [self updateItemIfNeeded:word];
            
        } else {
            [self.items addObject:word];
            [self loadNextSingleItemIfNeeded];
        }
    }
}

@end

//
//  TPResultsTableViewController+Notifications.h
//  textParser
//
//  Created by Ivan Bella López on 16/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "TPResultsTableViewController.h"

@interface TPResultsTableViewController (Notifications)

- (void)registerForNotifications;

@end

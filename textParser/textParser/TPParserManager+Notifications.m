//
//  TPParserManager+Notifications.m
//  textParser
//
//  Created by Ivan Bella López on 16/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "TPParserManager+Notifications.h"

@interface TPParserManager ()

- (void)resetParser;

@end


@implementation TPParserManager (Notifications)

- (void)registerForNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_resetParser:) name:TPNotificationWillAttemptLoad object:nil];
}

- (void)_resetParser:(NSNotification *)notification {
    [self resetParser];
}

@end

//
//  TPResultsTableViewController+DataPaginator.h
//  textParser
//
//  Created by Ivan Bella López on 14/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "TPResultsTableViewController.h"

@class TPWord;

@interface TPResultsTableViewController (DataPaginator)

- (void)loadItemsIfNeeded;
- (void)loadNextSingleItemIfNeeded;
- (void)updateItemIfNeeded:(TPWord *)word;
- (void)resetPaginator;

@end

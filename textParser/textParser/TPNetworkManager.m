//
//  TPNetworkManager.m
//  textParser
//
//  Created by Ivan Bella López on 13/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import "TPNetworkManager.h"
#import "TPNetworkManager+Notifications.h"
#import "TPNetworkManager+Errors.h"

@interface TPNetworkManager ()

@property (nonatomic, weak) id <TPNetworkManagerProtocol> delegate;
@property (nonatomic, strong) dispatch_queue_t serialQueue;

@end


@implementation TPNetworkManager

- (instancetype)initWithDelegate:(id<TPNetworkManagerProtocol>)delegate
{
    if (self = [super init]) {
        self.serialQueue = dispatch_queue_create("texparser.queue.network.manager", DISPATCH_QUEUE_SERIAL);
        
        self.delegate = delegate;
        [self registerForNotifications];
    }
    
    return self;
}


#pragma mark Public

- (void)requestWithURL:(NSString *)urlString {
    
    NSURL *URL = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:60.0];
    
    [request setValue:@"text/plain" forHTTPHeaderField:@"Accept"];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionTask *task = [session dataTaskWithRequest:request];
    [task resume];
}


#pragma mark NSURLSessionDataDelegate

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if (!error) {
        dispatch_async(self.serialQueue, ^{
            if ([self.delegate respondsToSelector:@selector(networkDidFinishReceivingData:)]) {
                [self.delegate networkDidFinishReceivingData:error];
            }
        });
    } else {
        [self showAlertWithError:error];
        
        dispatch_async(self.serialQueue, ^{
            if ([self.delegate respondsToSelector:@selector(networkURLNotValid)]) {
                [self.delegate networkURLNotValid];
            }
        });
    }
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data
{
    dispatch_async(self.serialQueue, ^{
        if ([self.delegate respondsToSelector:@selector(networkDidReceiveData:)]) {
            [self.delegate networkDidReceiveData:data];
        }
    });
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler {
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
    
    NSRange range = [[httpResponse allHeaderFields][@"Content-Type"] rangeOfString:@"text/plain"];
    
    if ([httpResponse statusCode] == 200 &&
        range.location != NSNotFound) {
        completionHandler(NSURLSessionResponseAllow);
    } else {
        NSError *error = [NSError errorWithDomain:@"error" code:[httpResponse statusCode] userInfo:nil];
        [self showAlertWithError:error];
        
        dispatch_async(self.serialQueue, ^{
            if ([self.delegate respondsToSelector:@selector(networkURLNotValid)]) {
                [self.delegate networkURLNotValid];
            }
        });
    }
}

@end

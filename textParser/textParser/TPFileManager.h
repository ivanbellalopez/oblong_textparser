//
//  TPFileManager.h
//  textParser
//
//  Created by Ivan Bella López on 15/06/15.
//  Copyright (c) 2015 Ivan Bella López. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TPAppController.h"

@protocol TPFileManagerDelegate <NSObject>

- (void)fileManagerDidReadData:(NSData *)data;

@end

@interface TPFileManager : NSObject

- (instancetype)initWithDelegate:(id <TPFileManagerDelegate>)delegate;
- (void)appendData:(NSData *)data;
- (NSArray *)listSavedFiles;

@end



